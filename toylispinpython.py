from collections import namedtuple


class Token:

    OpeningParen: str = 'OpeningParen'
    ClosingParen: str = 'ClosingParen'
    EverythingElse: str = 'EverythingElse'

    _value: str
    _kind: str

    def __init__(self,
                 value: str,
                 kind: str) -> None:

        self._value = value
        self._kind = kind

    def value(self) -> str:

        return self._value

    def is_opening_paren(self) -> bool:

        return self._kind == self.OpeningParen

    def is_closing_paren(self) -> bool:

        return self._kind == self.ClosingParen


def mktoken(character: str) -> Token:

    if character == '(':
        return Token('(', Token.OpeningParen)

    if character == ')':
        return Token(')', Token.ClosingParen)

    return Token(character, Token.EverythingElse)


tokens: list = [mktoken(char)
                for char in '( + 1 ( * 3 6 ) ( / 2 4 ( inc 10 ) ) )'.split(' ')]


class Literal:

    _token: Token

    def __init__(self,
                 token: Token) -> None:

        self._token = token

    def token(self) -> Token:

        return self._token

    def dump(self, indentation: int = 0) -> int:

        print((' ' * indentation if indentation else '') + self.token().value())

        return indentation


class Expression:

    _nodes: list

    def __init__(self,
                 nodes: list) -> None:

        self._nodes = nodes

    def nodes(self) -> list:

        return self._nodes

    def dump(self, indentation: int = 0) -> int:

        print((' ' * indentation if indentation else '') + '(')
        for node in self.nodes():
            updated_indentation = node.dump(indentation + 1)
        print((' ' * updated_indentation if updated_indentation else '') + ') ')

        return updated_indentation


# namedtuple module allows us create a tuple that acts like immutable C++ struct
ReadFuncResult = namedtuple('ReadFuncResult', 'expression cursor')


def read(tokens: list,
         cursor: int = 0) -> Expression:

    collected_nodes: list = []

    while cursor < len(tokens) - 1:

        current_token = tokens[cursor]

        if current_token.is_opening_paren():

            expression, cursor = read(tokens,
                                      cursor + 1)

            if expression:
                collected_nodes.append(expression)

        elif current_token.is_closing_paren():

            break

        else:

            collected_nodes.append(Literal(current_token))

        cursor += 1

    return ReadFuncResult(cursor=cursor, expression=Expression(collected_nodes))


read(tokens).expression.dump()  # read tokens and call dump on a root expression
